exports.files = {
  javascripts: {joinTo: 'app.js'},
  stylesheets: {joinTo: 'app.css'},
};

exports.plugins = {
  babel: {
    plugins: ["inferno", "transform-class-properties", "transform-object-rest-spread"]
  },
  sass: {
    mode: 'native',
    sourceMapEmbed: true
  }
}

exports.modules = {
  autoRequire: {
    'app.js': ['initialize'],
  },
};
