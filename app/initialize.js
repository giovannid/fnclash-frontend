import { BrowserRouter, Route, Link, Switch } from 'inferno-router';
import { initDevTools } from 'inferno-devtools';
import { render }    from 'inferno';

import ApiProvider   from './components/ApiProvider';
import ApiClient     from './components/ApiClient';

import Header        from './components/presentational/Header';
import Tournaments   from './components/containers/Tournaments';
import MyTournaments from './components/containers/MyTournaments';
import Login         from './components/containers/Login';
import Logout        from './components/containers/Logout';
import Register      from './components/containers/Register';
import Forgot        from './components/containers/Forgot';
import ResetPassword from './components/containers/ResetPassword';
import Profile       from './components/containers/Profile';
import NoMatch       from './components/presentational/NoMatch.js';

import PrivateRoute  from './components/PrivateRoute';
import CheckAuth     from './components/CheckAuth';

const client = new ApiClient({
    url: 'http://localhost:3000'
});

const Main = () => (
    <BrowserRouter>
        <ApiProvider client={client}>
            <Header/>
            <Switch>
                <PrivateRoute exact path="/" component={Tournaments} />
                <PrivateRoute path="/my-tournaments" component={MyTournaments} />
                <PrivateRoute path="/profile" component={Profile} />
                
                <CheckAuth path="/login" component={Login} />
                <CheckAuth path="/register" component={Register} />
                <CheckAuth path="/forgot-password" component={Forgot} />
                <CheckAuth path="/reset-password/:id" component={ResetPassword} />

                <PrivateRoute path="/logout" component={Logout} />

                <Route component={NoMatch} />
            </Switch>
        </ApiProvider>
    </BrowserRouter>
);

//initDevTools();
render(<Main />, document.getElementById('app'));