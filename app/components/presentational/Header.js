import { Link, NavLink } from 'inferno-router';
import { Component } from 'inferno';

import Query from '../Query';

class Header extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            credits: 0
        }
    }

    hideTourLink = (match, location) => {
        if(match.isExact || location.pathname == '/my_tournaments') {
            return true;
        }
    }

    componentWillMount() {
        this.setState({credits: this.context.client.getUserInfo('credits')});
    }

    render() {
        return (
            <div id="header">
                <Link to='/'>
                    <img class="logo" src="../images/logo-white.png" alt="Fortnite Clash"/>
                </Link>
                {this.context.client.isAuthorized()
                ? <ul class="nav">
                        <li><NavLink isActive={this.hideTourLink} activeStyle={{display: 'none'}} to='/'>Tournaments</NavLink></li>
                        <li><Link to='/profile/credits' class="credits">{this.state.credits}</Link></li>
                        <li><Link to='/profile' class="username">{this.context.client.getUserInfo('username')}</Link></li>
                        <li><Link to='/logout'>Logout</Link></li>
                  </ul>
                : <ul class="nav">
                      <li><Link to='/login'>Login</Link></li>
                      <li><Link to='/register'>Register</Link></li>
                  </ul>}
            </div>
        )
    }
}

export default Header;