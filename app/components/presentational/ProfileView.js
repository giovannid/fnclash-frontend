import { Component } from 'inferno';
import LoadingButton from '../presentational/LoadingButton';
import CustomForm    from './CustomForm';
import FormField     from './FormField';
import Mutation      from '../Mutation';
import moment        from 'moment';

export default class ProfileView extends Component {
    render() {
        const { info } = this.props;
        const resetFields = ['password'];
        return (
            <div class="ProfileView">
                <Mutation endpoint="/users/update">
                    {(execute, {loading, error, data}) => (
                        <CustomForm formSubmit={values => execute(values)}>
                            <FormField name="username" title="Username" type="text" disabled value={info.username} />
                            <FormField name="email" title="Email" type="email" value={info.email} />
                            <FormField name="password" title="New Password" type="password">
                                <div className="creationDate">
                                    <span>Member since: {moment.unix(info.creation).format("MMMM Do YYYY, h:mm:ss a")}</span>  
                                </div>
                            </FormField>
                            <LoadingButton name="Save Changes" onloading={loading} onerror={error} resetFields={resetFields}/>
                        </CustomForm>
                    )}
                </Mutation>
            </div>
        )
    }
}