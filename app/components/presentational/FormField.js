import { Component, linkEvent } from 'inferno';

export default class FormField extends Component {
    constructor(props, context) {
        super(props, context);
    }

    componentWillMount() {
        if(this.context.formApi.getValue(name) == '') {
            if(this.props.type == "radio" && this.props.checked !== true) return;
            this.updateForm(this.props.name, this.props.value)
        }
    }

    updateForm = (name, value) => {
        this.context.formApi.setValue(name, value);
    }

    render() {
        const { name, title, type = "text", value, children, classes, ...rest } = this.props;
        const valueFromForm = this.context.formApi.getValue(name);
        
        const onaction = this.props.type == "radio" 
        ? { onChange: linkEvent(this, handleCheckbox) } 
        : { onInput:  linkEvent(this, handleInput) }

        return (
            <div class={classes}>
                <label>
                    {title 
                        ? <span>{title}:</span> 
                        : ''}
                    <input type={type} value={valueFromForm} name={name} {...rest} {...onaction} formNoValidate />
                    {children}
                </label>
            </div>
        )
    }
}

function handleCheckbox(instance, event) {
    instance.updateForm(event.target.name, instance.props.value);
}

function handleInput(instance, event) {
    instance.updateForm(event.target.name, event.target.value);
}
