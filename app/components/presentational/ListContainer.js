import { linkEvent, Component } from 'inferno';
import ListContainerItem from './ListContainerItem';

class ListContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            prize: true,
            slots: true,
            time: true,
            fee: true,
            currentPage: 1,
            perPage: 4
        }
    }

    filterby = (instance, event) => {
        let filter = event.target.dataset.filterby;
        instance.props.filterby(instance.props.list, filter, instance.state[filter]);
        instance.setState({ [filter]: !instance.state[filter] , currentPage: 1 });
    }

    changePage = (instance, event) => {
        this.setState({ currentPage: Number(event.target.id) });
    }

    render() {
        const { name, inprogress, source } = this.props;

        const indexOfLastItem = this.state.currentPage * this.state.perPage;
        const indexOfFirstItem = indexOfLastItem - this.state.perPage;
        const currentItems = source.slice(indexOfFirstItem, indexOfLastItem);

        const pageNumbers = [];
        for(let i = 1; i <= Math.ceil(source.length / this.state.perPage); i++) {
            pageNumbers.push(i);
        }

        const renderPageNumbers = pageNumbers.map(number => (
            <li key={number} id={number} className={ (this.state.currentPage == number) ? 'active' : '' } onClick={ linkEvent(this, this.changePage) }>{number}</li>
        ));
        
        return (
            <div class="ListContainer">
                    <div class="top_bar grid-system">
                        <span><button class="name">{name}</button></span>
                        { !inprogress 
                            ? <span><button data-filterby="time"  className={ (this.state.time ? 'asc' : 'desc') + ' time' }   onClick={ linkEvent(this, this.filterby) }>Starts In</button></span>
                            : <span></span> }
                        <span><button data-filterby="prize" className={ (this.state.prize ? 'asc' : 'desc') + ' prize' } onClick={ linkEvent(this, this.filterby) }>Prize Pool</button></span>
                        <span><button data-filterby="slots" className={ (this.state.slots ? 'asc' : 'desc') + ' slots' } onClick={ linkEvent(this, this.filterby) }>Slots</button></span>
                        <span><button data-filterby="fee" className={ (this.state.fee ? 'asc' : 'desc') + ' fee' } onClick={ linkEvent(this, this.filterby) }>Entrance Fee</button></span>
                    </div>
                <div className="ListContainerChildren">
                    {currentItems.map(item =>
                        <ListContainerItem 
                            key={`${item.id}`}
                            name={`${item.name}`}
                            queuetype={`${item.queuetype}`}
                            time={`${item.time}`}
                            prize_pool={`${item.prize_pool}`}
                            max_players={`${item.max_players}`}
                            registered={`${item.registered}`}
                            id={`${item.id}`}
                            entrance_fee={`${item.entrance_fee}`}
                            inprogress={inprogress} />
                    )}
                </div>
                <ul class="pagination">{renderPageNumbers}</ul>
            </div>
        )
    }
}

export default ListContainer;
