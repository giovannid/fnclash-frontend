import { Component, linkEvent } from 'inferno';

export default class CustomForm extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            data: {} 
        }
    }

    resetField = (name) => {
        if(this.state.data.hasOwnProperty(name)){
            this.setState(prevState => ({
                data: Object.assign(prevState.data, {
                    [name]: ''
                })
            }));
        }
    }
 
    setValue = (name, value = null) => {
        this.setState(prevState => ({
            data: Object.assign(prevState.data, {
                [name]: value
            })
        }));
    }

    getValue = (name) => {
        return this.state.data[name] || '';
    }

    getChildContext () {
        return {
            formApi: {
                setValue: this.setValue,
                getValue: this.getValue,
                resetField: this.resetField
            }
        }
    }

    render() {
        return (
            <div class="customForm">
                <form onSubmit={ linkEvent(this, handleSubmit) }>
                    {this.props.children}
                </form>
            </div>  
        )
    }
}

function handleSubmit(instance, event) {
    event.preventDefault();
    instance.props.formSubmit(instance.state.data);
}