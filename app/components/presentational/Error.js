const Error = (props) => {
    const { message = "Error. Try Again." } = props;
    return (
        <div class="ErrorDiv">
        <div class="ErrorContent">
            <i class="fas fa-exclamation-triangle"></i>
            <span>{message}</span>
        </div>
    </div>
    );
}

export default Error;