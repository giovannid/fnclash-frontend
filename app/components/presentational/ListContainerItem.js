import { Component } from 'inferno';

import moment from 'moment';

class ListContainerItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            time_remaining: ""
        };
    }

    componentDidMount() {
        this.updateTime();
        this.refreshTime = setInterval(() => {
            this.updateTime();
        }, 60 * 1000);
    }

    updateTime() {
        let timenow = moment();
        let startsin = moment.unix(this.props.time);
        this.setState({
            time_remaining: timenow.to(startsin) 
        });
    }
  
    componentWillUnmount() {
        clearInterval(this.refreshTime);
    }

    render() {
        const { name, queuetype, time, prize_pool, max_players, registered, id, inprogress = false, key, entrance_fee } = this.props;
        let itemClass = 'ListContainerItem';
        if(inprogress) itemClass += ' inprogress';
        
        return (
            <div className={itemClass} key={key}>
                <div class="item_info grid-system">
                    
                    <span class="name">
                        <span class="queuetype">{queuetype}</span>
                        {name}
                    </span>
                    <span class="time">{ inprogress ? 'Started' : this.state.time_remaining }</span>
                    <span>{prize_pool}cc</span>
                    <span>{registered}/{max_players}</span>
                    <span>{entrance_fee}cc</span>
                    <span>
                        <div class="button_area">
                            {!inprogress 
                                ?  <span class="button">Register Now</span>
                                :  <span class="button">View</span>
                            }
                        </div>
                    </span>
                </div>
            </div>
        )
    }
}

export default ListContainerItem;
