const ContentMenu = ({children}) => (
    <div class="ContentMenu">
        {children}
    </div>
)

export default ContentMenu;
