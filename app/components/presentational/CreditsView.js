import LoadingButton from '../presentational/LoadingButton';
import CustomForm    from '../presentational/CustomForm';
import FormField     from '../presentational/FormField';
import Mutation      from '../Mutation';

const CreditsView = (props) => {
    const { credits } = props;
    return (
        <div class="CreditsView">
            <div className="coinInfo">
                <div class="currentCredits">
                    <p>CLASH COINS AVAILABLE</p>
                    <div class="credits">{credits}<span>cc</span></div>

                    <p>CLASH COINS AVAILABLE FOR WITHDRAW</p>
                    <div class="credits">{credits}<span>cc</span></div>

                    <button class="withdraw">WITHDRAW</button>
                </div>
            </div>

            <CustomForm formSubmit={values => console.log(values)}>
                <div class="grouped_options">
                    <FormField classes="credit_options" name="ccamount" type="radio" value="20" checked>
                        <div class="highlight"></div>
                        <p class="credits">20<span>cc</span></p>
                        <p class="price">$5.99</p>
                        <div class="circle"></div>
                        
                    </FormField>
                    <FormField classes="credit_options" name="ccamount" type="radio" value="50">
                        <div class="highlight"></div>
                        <p class="credits">50<span>cc</span></p>
                        <p class="price">$15.99</p>
                        <div class="circle"></div>
                        
                    </FormField>

                    <FormField classes="credit_options" name="ccamount" type="radio" value="100">
                        <div class="highlight"></div>
                        <p class="credits">100<span>cc</span></p>
                        <p class="price">$55.99</p>
                        <div class="circle"></div>
                        
                    </FormField>

                    <FormField classes="credit_options" name="ccamount" type="radio" value="150">
                        <div class="highlight"></div>
                        <p class="credits">150<span>cc</span></p>
                        <p class="price">$95.99</p>
                        <div class="circle"></div>
                        
                    </FormField>

                    <FormField classes="credit_options" name="ccamount" type="radio" value="200">
                        <div class="highlight"></div>
                        <p class="credits">200<span>cc</span></p>
                        <p class="price">$155.99</p>
                        <div class="circle"></div>
                        
                    </FormField>
                    
                </div>
                
                <FormField classes="credits_button" type="submit" name="submit" value="Buy More Coins" />
            </CustomForm>
        </div>
    )
}

export default CreditsView;