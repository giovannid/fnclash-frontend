import { Component, linkEvent } from 'inferno';

export default class LoadingButton extends Component {
    constructor(props, context){
        super(props, context);
        this.state = {
            loading: false,
            error: false,
            success: false
        }
        this.svg_progress = 0;
    }

    enableButton = () => {
        this.setState({ 
            success: false, 
            error: false, 
            loading: false 
        }, () => this.button.removeAttribute('disabled'));
    }

    resetFields = (fields = null) => {
        if(!fields) return;
        fields.map((name) => this.context.formApi.resetField(name));
    }

    onDone = () => {
        setTimeout(() => {
            if(this.props.ondone !== undefined) this.props.ondone()
            else this.enableButton();
        }, 1000);
    }

    refreshButton = () => {
        setTimeout(() => this.enableButton(), 1000);
    }

    setProgress = (val) => {
        let path = this.progress.querySelector('path');
        path.style.strokeDashoffset = path.getTotalLength() * ( 1 - val );
    }

    checkForLoading = () => {
        this.inverval = setInterval(() => {
            if(this.props.onloading == true) {
                this.svg_progress = Math.min( this.svg_progress + Math.random() * 0.1, 1 );
			    this.setProgress(this.svg_progress);
            } else {
                if(this.props.onerror === false) {
                    this.setProgress(1);
                    clearInterval(this.inverval);
                    setTimeout(() => {
                        this.setState({
                            loading: false,
                            error: false,
                            success: true
                        }, () => {
                            this.resetFields(this.props.resetFields);
                            this.onDone();
                        });
                    }, 500);
                } else {
                    this.setState({
                        loading: false,
                        error: true,
                        success: false
                    }, () => {
                        clearInterval(this.inverval);
                        this.resetFields(this.props.resetFields);
                        this.refreshButton();
                    });
                }
            }
        }, 500);
    }

    render() {
        const { name } = this.props;
        const { loading, error, success } = this.state
        const loadingclass = loading ? ' loading '  : '';
        const errorclass   = error   ? ' error '    : '';
        const successclass = success ? ' success '  : '';
        return (
            <div className={ 'progress-button ' + loadingclass + errorclass + successclass }>
                <button type="submit" formNoValidate onClick={ linkEvent(this, handleClick) } ref={ button => { this.button = button } }><span>{name}</span></button>
                <svg width="42" height="42" class="progress-circle" ref={ svg => { this.progress = svg } }>
                    <path d="M19.5,0A19.5,19.5,0,1,1,0,19.5,19.5,19.5,0,0,1,19.5,0Z" transform="translate(1.5, 1.5)"></path>
                </svg>
                <i class="far fa-check checkmark"></i>
                <i class="far fa-times cross"></i>
            </div>
        )
    }
}

function handleClick(instance, event) {
    setTimeout(() => {
        instance.button.setAttribute('disabled', 'true');
    }, 0);
    
    let path = instance.progress.querySelector('path')
    let length = path.getTotalLength();

    path.style.strokeDasharray = `${length},${length}`;
    path.style.strokeDashoffset = length;
    path.getBoundingClientRect();
    
    instance.checkForLoading();
    instance.setState({loading: true});
}