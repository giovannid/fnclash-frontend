import { Component } from 'inferno';

export default class Query extends Component{
    constructor(props, context) {
        super(props, context)
        this.client = context.client;

        this.state = {
            loading: true,
            error: false,
            data: {}
        }
    }

    componentDidMount() {
        this.executeQuery(this.props);
    }
    
    executeQuery(props) {
        const { endpoint, method = 'GET' } = props;

        this.client.request(endpoint, method)
        .then(res => res.json())
        .then(result => {
            this.setState({
                loading: false,
                data: result
            })
        })
        .catch(err => {
            console.log(err);
            this.setState({
                loading: false,
                error: err
            });
        })
    }

    render() {
        return this.props.children(this.state);
    }
}