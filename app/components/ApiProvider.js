import { Component } from 'inferno';
import Cookies from 'js-cookie';

import Loading from './presentational/Loading';

export default class ApiProvider extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            loading: true
        }
    }

    componentWillMount() {
        let session = Cookies.get('session');
        if(session) {
            this.props.client.saveTokenInfo(session);
            this.props.client.request('/users/me')
            .then(res => res.json())
            .then(res => {
                if(res.code == 200) {
                    this.props.client.saveUserInfo(res.result, () => {
                        this.setState({loading: false})
                    });
                }
            })
        } else {
            this.props.client.delTokenInfo();
            this.setState({loading: false});
        }
    }

    getChildContext() {
        return {
          client: this.props.client
        };
    }

    render() {
        const { loading } = this.state;

        if(loading) {
            return (
                <div>
                    <div id="header">
                        <img class="logo" src="../images/logo-white.png" alt="Fortnite Clash"/>
                    </div>
                    <div class="main_content"> 
                        <Loading /> 
                    </div>
                </div>
            )
        }
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}