import moment from 'moment';

function DummyServer(endpoint, options) {
    function returnPromise(out) {
        return new Promise(resolve => {
            setTimeout(_ => {
                resolve({ json: () => out });
            }, 500);
        });
    }

    let data = {
        test: {
            id: 1,
            role: 'admin',
            credits: 999, 
            creation: '1553596097',  
            password: 'test',
            email: 'test@test.com',
            teams: {},
            username: 'test'
        }
    }

    console.log(endpoint, options);
    let body = options.body && JSON.parse(options.body);
    let method = options.method;

    switch(endpoint) {
        case `/users/auth`:
            console.log(`check db: ${JSON.stringify(data)}`);
            if(body.username && body.password && body.username === data.test.username && body.password === data.test.password) {
                return returnPromise({ code: 200, result: { token: 'AAAAA', userinfo: data.test }, message: '' });
            }
            return returnPromise({ code: 401, result: { error: true }, message: 'Missing Username and/or Password' });
        case '/info/test':
            return returnPromise({ code: 200, result: { token: 'AAAAA', userinfo: data.test }, message: '' });
        case '/users/me':
            return returnPromise({ code: 200, result: data.test, message: '' });
        case '/users/forgot':
            if(body.email === data.test.email) {
                return returnPromise({ code: 200, result: { resetkey_created: true }, message: '' });
            }
            return returnPromise({ code: 401, result: { error: true }, message: 'Email not found.' });
        case '/users/register':
            if(body.username && body.password && body.email ) {
                if(body.email === data.test.email) {
                    return returnPromise({ code: 201, result: { account_exists: true }, message: 'Username/Email already being used by an account.' });
                } 
                return returnPromise({ code: 200, result: { user_registered: true }, message: '' });
            }
            return returnPromise({ code: 401, result: { error: true }, message: 'Required Fields Missing.' });
        case '/users/update':
            return returnPromise({ code: 200, result: { user_updated: true }, message: '' });
        case '/users/reset/test':
            console.log(``);
            if(method === 'GET') {
                return returnPromise({ code: 200, result: { valid_token: true } || {}, message: '' });
            } else {
                if(body.newpassword !== null && body.confirmpassword !== null) {
                    return returnPromise({ code: 200, result: { password_changed: true } || {}, message: '' });
                }
                return returnPromise({ code: 401, result: { error: true }, message: 'Required fields missing.' });
            }
        default: 
            return returnPromise({ code: 404, result: { error: true }, message: 'Not found!' });
    }
}

export default DummyServer;