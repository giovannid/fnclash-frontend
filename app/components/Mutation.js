import { Component } from 'inferno';

export default class Query extends Component{
    constructor(props, context) {
        super(props, context);
        this.client = context.client;

        this.state = {
            loading: false,
            error: false,
            data: {}
        }
    }

    executeMutation = (values) => {
        this.setState({loading: true, error: false});

        const { endpoint, method = 'POST', oncompleted, onerror } = this.props;

        this.client.request(endpoint, method, values)
        .then(res => res.json())
        .then(result => {
            if(result.code !== 200) {
                this.setState({
                    loading: false,
                    error: true,
                    data: result
                }, () => typeof onerror == 'function' && onerror(result));
            } else {
                this.setState({
                    loading: false,
                    error: false,
                    data: result
                }, () => typeof oncompleted === 'function' && oncompleted(result));
            }
        })
        .catch(err => {
            this.setState({
                loading: false,
                error: true
            }, () => typeof onerror == 'function' && onerror('Server out of reach!'));
        })
    }

    render() {
        return this.props.children(this.executeMutation, this.state);
    }
}