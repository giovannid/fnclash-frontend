import { Component } from 'inferno';
import { NavLink } from 'inferno-router';
import moment from 'moment';

import ContentMenu from '../presentational/ContentMenu';
import ListContainer from '../presentational/ListContainer';

class Tournaments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tournaments_list: [],
            tournaments_inprogress: []
        }
    }

    componentWillMount() {
        this.setState({ tournaments_list: this.tour_new, tournaments_inprogress: this.tour_inp });
    }

    filterby = (list, type, order = false) => {
        let filtered = [];
        switch (type) {
            case "prize":
                filtered = this.state[list].sort((a, b) => a.prize_pool - b.prize_pool);
                break;
            case "fee":
                filtered = this.state[list].sort((a, b) => a.entrance_fee - b.entrance_fee);
                break;
            case "slots":
                filtered = this.state[list].sort((a, b) => {
                    if( a.max_players == b.max_players ) {
                        if( a.registered > b.registered ) {
                            return -1;
                        } else {
                            return 1;
                        }
                    } else if( a.max_players > b.max_players ) {
                        return -1;
                    } else {
                        return 1;
                    }
                });
                break;
            case "time":
                filtered = this.state[list].sort((a, b) => {
                    let ma = moment.unix(a.time);
                    let mb = moment.unix(b.time);
                    if(ma > mb) {
                        return -1;
                    } else {
                        return 1;
                    }
                    return 0;
                });
                break;
            default:
                filtered = this.state[list];
                break;
        }
        if(order != true) filtered.reverse();
        this.setState({[list]: filtered});
    }

    tour_new = [
        { name: 'Free-4-bla', queuetype: "Solo", time: "1522291563", prize_pool: 9, max_players: 30, registered: 15, id: "121", entrance_fee: 300 },
        { name: 'Free-4-kha', queuetype: "Solo", time: "1522299789", prize_pool: 2, max_players: 30, registered: 10, id: "122", entrance_fee: 200 },
        { name: 'Free-4-siv', queuetype: "Solo", time: "1522310589", prize_pool: 4, max_players: 30, registered: 5, id: "123", entrance_fee: 500 },
        { name: 'Free-4-sio', queuetype: "Solo", time: "1522317789", prize_pool: 8, max_players: 50, registered: 50, id: "124", entrance_fee: 400 },
        { name: 'Free-4-xer', queuetype: "Solo", time: "1522284853", prize_pool: 6, max_players: 50, registered: 20, id: "125", entrance_fee: 700 },
        { name: 'Free-4-mal', queuetype: "Solo", time: "1522314189", prize_pool: 3, max_players: 50, registered: 5, id: "126", entrance_fee: 900 },
        { name: 'Free-4-ahr', queuetype: "Solo", time: "1522303389", prize_pool: 1, max_players: 80, registered: 80, id: "127", entrance_fee: 800 },
        { name: 'Free-4-lux', queuetype: "Solo", time: "1522321389", prize_pool: 7, max_players: 80, registered: 1, id: "128", entrance_fee: 100 },
        { name: 'Free-4-mor', queuetype: "Solo", time: "1522296189", prize_pool: 5, max_players: 80, registered: 50, id: "129", entrance_fee: 600 }        
    ];

    tour_inp = [
        { name: 'Free-4-All', queuetype: "Solo", time: "1522296189", prize_pool: 1, max_players: 30, registered: 20, id: "121", inprogress: true, entrance_fee: 300 },
        { name: 'Free-4-All', queuetype: "Solo", time: "1522299789", prize_pool: 2, max_players: 30, registered: 20, id: "122", inprogress: true, entrance_fee: 300 },
        { name: 'Free-4-All', queuetype: "Solo", time: "1522303389", prize_pool: 3, max_players: 30, registered: 20, id: "123", inprogress: true, entrance_fee: 300 },
        { name: 'Free-4-All', queuetype: "Solo", time: "1522306989", prize_pool: 4, max_players: 30, registered: 20, id: "124", inprogress: true, entrance_fee: 300 },
        { name: 'Free-4-All', queuetype: "Solo", time: "1522310589", prize_pool: 5, max_players: 30, registered: 20, id: "125", inprogress: true, entrance_fee: 300 },
        { name: 'Free-4-All', queuetype: "Solo", time: "1522314189", prize_pool: 6, max_players: 30, registered: 20, id: "126", inprogress: true, entrance_fee: 300 },
        { name: 'Free-4-All', queuetype: "Solo", time: "1522317789", prize_pool: 7, max_players: 30, registered: 20, id: "127", inprogress: true, entrance_fee: 300 },
        { name: 'Free-4-All', queuetype: "Solo", time: "1522321389", prize_pool: 8, max_players: 30, registered: 20, id: "128", inprogress: true, entrance_fee: 300 },
        { name: 'Free-4-All', queuetype: "Solo", time: "1522324989", prize_pool: 9, max_players: 30, registered: 20, id: "129", inprogress: true, entrance_fee: 300 },
    ];

    render() {
        return (
            <div class="main_content">
                <ContentMenu>
                    <NavLink exact to='/' activeClassName="activated" className="tournaments">Tournaments</NavLink>
                    <NavLink to='/my-tournaments' activeClassName="activated" className="my_tournaments">My Tournaments</NavLink>
                </ContentMenu>
                <ListContainer name="Top Tournaments" list="tournaments_list" source={this.state.tournaments_list} filterby={this.filterby} />
                <ListContainer name="TOURNAMENTS IN PROGRESS" list="tournaments_inprogress" source={this.state.tournaments_inprogress} filterby={this.filterby} inprogress={true} />
            </div>
        )
    }
}

export default Tournaments;