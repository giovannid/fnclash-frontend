import { Link, Redirect } from 'inferno-router';
import { Component }      from 'inferno';
import LoadingButton      from '../presentational/LoadingButton';
import CustomForm         from '../presentational/CustomForm';
import FormField          from '../presentational/FormField';
import Loading            from '../presentational/Loading';
import Error              from '../presentational/Error';
import Mutation           from '../Mutation';
import Query              from '../Query';

export default class ResetPassword extends Component {
    render() {
        const resetFields = ['newpassword', 'confirmpassword'];
        const resetkey = this.props.match.params.id;
        return (
            <div class="main_content">
                <div className="SingleFormContainer">
                    <Query endpoint={`/users/reset/${resetkey}`}>
                        {({loading, error, data}) => {
                            if (loading) return <Loading />;
                            if (error) return <Error />;
                            
                            if(data.result.valid_token === true) {
                                return (
                                    <Mutation endpoint={`/users/reset/${resetkey}`}>
                                        {(execute, {loading, error}) => (
                                            <CustomForm formSubmit={values => execute(values)}>
                                                <div className="title">
                                                    <h1>Reset your password</h1>
                                                </div>
                                            
                                                <FormField name="newpassword" title="New Password" type="password" />
                                                <FormField name="confirmpassword" title="Confirm Password" type="password" />
                                                
                                                <LoadingButton name="Reset Password" onloading={loading} onerror={error} resetFields={resetFields} />
                                            </CustomForm>
                                        )}
                                    </Mutation>
                                );
                            }
                            return <Redirect to="/login" />;
                        }}
                    </Query>
                </div>
            </div>
        )
    }
}