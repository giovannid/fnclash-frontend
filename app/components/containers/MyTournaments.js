import { Component } from 'inferno';
import { NavLink } from 'inferno-router';

import ContentMenu from '../presentational/ContentMenu';

class Tournaments extends Component {
    constructor(props) {
        super(props);
        
    }

    render() {
        return (
            <div class="main_content">
                <ContentMenu>
                    <NavLink exact to='/' activeClassName="activated" className="tournaments">Tournaments</NavLink>
                    <NavLink to='/my_tournaments' activeClassName="activated" className="my_tournaments">My Tournaments</NavLink>
                </ContentMenu>
                <h1>My Tournaments</h1>
            </div>
        )
    }
}

export default Tournaments;