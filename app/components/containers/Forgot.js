import { Component } from 'inferno';
import { Link } from 'inferno-router';

import LoadingButton from '../presentational/LoadingButton';
import CustomForm    from '../presentational/CustomForm';
import FormField     from '../presentational/FormField';
import Mutation      from '../Mutation';

export default class Forgot extends Component {
    render() {
        const resetFields = ['email'];
        
        return (
            <div class="main_content">
                <div className="SingleFormContainer">
                    <Mutation endpoint="/users/forgot">
                        {(execute, {loading, error, data}) => (
                            <CustomForm formSubmit={values => execute(values)}>
                                <div className="title">
                                    <h1>Forgot your password?</h1>
                                </div>

                                <FormField name="email" title="Email (test@test.com)" type="email" />
                                <LoadingButton name="Send Reset Link" onloading={loading} onerror={error} resetFields={resetFields} />

                                <div className="noaccount">
                                    <span>Already have an account? <Link to='/login' class="signup">Login</Link></span>
                                </div>
                            </CustomForm>
                        )}
                    </Mutation>
                </div>
            </div>
        )
    }
}