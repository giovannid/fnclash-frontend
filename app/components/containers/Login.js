import { Component } from 'inferno';
import { Link, Redirect } from 'inferno-router';

import CustomForm    from '../presentational/CustomForm';
import FormField     from '../presentational/FormField';
import LoadingButton from '../presentational/LoadingButton';

import Mutation      from '../Mutation';


export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirectToReferrer: false
        }
    }

    saveLoginState = (data) => {
        this.context.client.saveFirstLogin(data.result.token, data.result.userinfo);
    }

    ondone = () => {
        this.setState({ redirectToReferrer: true });
    }

    render() {

        const { from } = this.props.location.state || { from: { pathname: "/" } };
        const { redirectToReferrer } = this.state;

        if (redirectToReferrer) {
            return <Redirect to={from} />;
        }

        return (
            <div class="main_content">
                <div className="SingleFormContainer">
                    <Mutation endpoint="/users/auth" oncompleted={this.saveLoginState}>
                        {(execute, {loading, error, data}) => (
                            <CustomForm formSubmit={values => execute(values)}>
                                <div className="title">
                                    <h1>Welcome Back!</h1>
                                </div>

                                <FormField name="username" title="Username (test)" type="text" required autofocus />

                                <FormField name="password" title="Password (test)" type="password" required>
                                    <Link to='/forgot-password' class="forgot">Forgot your password?</Link>
                                </FormField>
                                
                                <LoadingButton name="Login" onloading={loading} onerror={error} ondone={this.ondone} />

                                <div className="noaccount">
                                    <span>Don't have an account? <Link to='/register' class="signup">Sign up</Link></span>
                                </div>
                            </CustomForm>
                        )}
                    </Mutation>
                </div>
            </div>
        )
    }
}