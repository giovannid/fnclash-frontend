import { Component } from 'inferno';
import { Redirect } from 'inferno-router';

const Logout = (props, context) => {
    context.client.delTokenInfo();
    return <Redirect to="/login" />
}

export default Logout;