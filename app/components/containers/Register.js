import { Component } from 'inferno';
import { Link }      from 'inferno-router';

import LoadingButton from '../presentational/LoadingButton';
import CustomForm    from '../presentational/CustomForm';
import FormField     from '../presentational/FormField';
import Mutation      from '../Mutation';

export default class Register extends Component {
    render() {
        const resetFields = ['username', 'email', 'password'];
        return (
            <div class="main_content">
                <div className="SingleFormContainer">
                    <Mutation endpoint="/users/register">
                        {(execute, {loading, error, data}) => (
                            <CustomForm formSubmit={values => execute(values)}>
                                <div className="title">
                                    <h1>Create your account</h1>
                                </div>
                                <FormField name="username" title="Username" type="text" />
                                <FormField name="email" title="Email" type="email" />
                                <FormField name="password" title="Password" type="password" />
                                
                                <LoadingButton name="Get Started" onloading={loading} onerror={error} resetFields={resetFields} />
                                
                                <div className="noaccount">
                                    <span>Already have an account? <Link to='/login' class="signup">Login</Link></span>
                                </div>
                            </CustomForm>
                        )}
                    </Mutation>
                </div>
            </div>
        )
    }
}
