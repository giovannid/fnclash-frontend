import { Component, linkEvent } from 'inferno';
import { Route, NavLink } from 'inferno-router';

import ContentMenu from '../presentational/ContentMenu';
import ProfileView from '../presentational/ProfileView';
import CreditsView from '../presentational/CreditsView';

import Loading     from '../presentational/Loading';
import Error       from '../presentational/Error';

import Query       from '../Query';

export default class Profile extends Component {
    render() {
        return (
            <div class="main_content">
                <ContentMenu>
                <NavLink to='/profile/credits' activeClassName="activated" className="clash_coins">Clash Coins</NavLink>
                    <NavLink exact to='/profile' activeClassName="activated" className="profile">Profile</NavLink>
                </ContentMenu>
                
                <Query endpoint="/users/me" method="get">
                    {({loading, error, data}) => {
                        if (loading) return <Loading />;
                        if (error) return <Error />;
                        
                        return <div>
                            <Route  exact 
                                    path={this.props.match.url}
                                    render={props => <ProfileView info={data.result} {...props} />} />
                            <Route  path="/profile/credits" 
                                    render={props => <CreditsView credits={data.result.credits} {...props} />} />
                        </div>;
                    }}
                </Query>
            </div>
        )
    }
}