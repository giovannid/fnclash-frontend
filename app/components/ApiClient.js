import 'whatwg-fetch';
import Cookies from 'js-cookie';
import moment from 'moment';
import DummyServer from './DummyServer';

export default class ApiClient {
    constructor(args) {
        this._api = args.url
        this.authorized = false;
        this.authorization_header = '';
        this.userinfo = {};
        this.listeners = {};
    }

    request(endpoint, method = 'GET', data = {}) {
        let options = {}
        options.method = method;
        options.headers = {
            'Authorization': `bearer ${this.authorization_header}`
        }
        if(method.toUpperCase() !== 'GET') {
            options.body = JSON.stringify(data);
            options.headers = Object.assign({}, options.headers, {
                'Content-Type': 'application/json'
            })
        }
        console.log(`fetching: ${this._api}${endpoint}`);
        // return fetch(`${this._api}${endpoint}`, options);
        return DummyServer(endpoint, options);
    }

    delTokenInfo() {
        this.authorized = false;
        this.authorization_header = '';
        Cookies.remove('session');
    }

    isAuthorized() {
        return this.authorized;
    }

    saveTokenInfo(token) {
        this.authorized = true;
        this.authorization_header = token;
    }

    saveUserInfo(userinfo, callback) {
        this.userinfo = userinfo;
        typeof callback == "function" && callback();
    }

    getUserInfo(key) {
        return this.userinfo[key] || '';
    }

    saveFirstLogin(token, userinfo, callback) {
        this.saveTokenInfo(token);
        this.saveUserInfo(userinfo);
        Cookies.set('session', token, { expires: 365 });
        typeof callback == "function" && callback();
    }

    addListener(endpoint, callback) {
        let id = Math.floor( Math.random() * moment().unix() );
        if (this.listeners.hasOwnProperty(endpoint)) {
            this.listeners[endpoint].set(id, callback);
        } else {
            this.listeners[endpoint] = new Map().set(id, callback);
        }
        return id;
    }

    removeListener(endpoint, id) {
        this.listeners[endpoint] && this.listeners[endpoint].delete(id);
    }

    warnListeners(endpoint, newValue) {
        if(this.listeners[endpoint]) {
            for(let func of this.listeners[endpoint].values()) func(newValue);
        }
    }

}