import { Component } from 'inferno';
import { Route, Redirect } from 'inferno-router';

export default class CheckAuth extends Component{
    constructor(props, context) {
        super(props, context);
    }

    render() {
        const isAuthenticated = this.context.client.isAuthorized();
        const { component: Component, ...rest } = this.props;
        const { from } = this.props.location.state || { from: { pathname: "/" } };
        
        return <Route {...rest} render={props => (
            isAuthenticated 
            ? <Redirect to={from} /> 
            : <Component {...props} />
        )} />
    }
}