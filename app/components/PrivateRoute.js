import { Component } from 'inferno';
import {  Route, Redirect } from 'inferno-router';

export default class PrivateRoute extends Component{
    constructor(props, context) {
        super(props, context);
    }

    render() {
        const isAuthenticated = this.context.client.isAuthorized();
        const { component: Component, ...rest } = this.props;
        return <Route {...rest} render={props => (
            isAuthenticated 
            ? <Component {...props} /> 
            : <Redirect to={{pathname: '/login', state: { from: props.location }}} />
        )} />
    }
}